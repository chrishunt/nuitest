package KinectTesting;

import com.sun.java.swing.plaf.motif.MotifLookAndFeel;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.io.IOException;
import javax.swing.*;

import org.openni.Device;
import org.openni.OpenNI;

import org.openni.*;
import com.primesense.nite.*;

import javax.swing.JOptionPane;

public class UserViewerApplication implements ActionListener 
{

    private JFrame mFrame;
    private UserViewer mViewer;
    private boolean mShouldRun = true;
    private boolean handTrack;
    Button serializeButton, saveXMLButton;

    public UserViewerApplication(UserTracker tracker) 
    {
        mFrame = new JFrame("NiTE User Tracker Viewer");
        mViewer = new UserViewer(tracker);
        
        // register to key events
        mFrame.addKeyListener(new KeyListener() 
        {
            @Override
            public void keyTyped(KeyEvent arg0) {}
            
            @Override
            public void keyReleased(KeyEvent arg0) {}
            
            @Override
            public void keyPressed(KeyEvent arg0) 
            {
                if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) 
                {
                    mShouldRun = false;
                }
            }
        });
        
        // register to closing event
        mFrame.addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent e) 
            {
                mShouldRun = false;
            }
        });
        Panel p1 = new Panel(new GridLayout(1,2));
        serializeButton = new Button("Serialize");
        serializeButton.setActionCommand("serialize");
        serializeButton.addActionListener(this);
        p1.add(serializeButton);
        saveXMLButton = new Button("Save XML");
        saveXMLButton.setActionCommand("savexml");
        saveXMLButton.addActionListener(this);
        p1.add(saveXMLButton);
        mFrame.add("South",p1);

        mViewer.setSize(800, 600);
        mFrame.add("Center", mViewer);
        mFrame.setSize(mViewer.getWidth(), mViewer.getHeight());
        mFrame.setVisible(true);
    }

    public boolean serialiseSkeletons()
    {
        boolean success = true;
        try
        {
            mViewer.serialize();
        }
        catch (IOException ie)
        {
            success = false;
        }
        return success;
    }

    public void saveSkeletonsToXML()
    {
        mViewer.xmlSave();
    }

    public void actionPerformed(ActionEvent e) 
    {
        if (e.getActionCommand().equalsIgnoreCase("serialize")) 
        {
            boolean success = serialiseSkeletons();
            if (success)
                JOptionPane.showMessageDialog(null, "Saved serialied skeletal position data.", "InfoBox: ", JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "Error while serializing skeletal position data.", "InfoBox: ", JOptionPane.INFORMATION_MESSAGE);

        } 
        else if (e.getActionCommand().equalsIgnoreCase("savexml")) 
        {
            saveSkeletonsToXML();
            JOptionPane.showMessageDialog(null, "Skeletal position data saved as XML.", "InfoBox: ", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    void run() 
    {
        while (mShouldRun) 
        {
            try 
            {
                Thread.sleep(200);
            } 
            catch (InterruptedException e) 
            {
                e.printStackTrace();
            }
        }
        mFrame.dispose();
    }

    static 
    {
        System.load("C:\\Program Files\\OpenNI2\\Redist\\OpenNI2.dll");
        System.load("C:\\Program Files\\PrimeSense\\NiTE2\\Redist\\NiTE2.dll");
    }

    public static void main(String s[]) 
    {
        // initialize OpenNI and NiTE
    	OpenNI.initialize();
        NiTE.initialize();
        
        List<DeviceInfo> devicesInfo = OpenNI.enumerateDevices();
        if (devicesInfo.size() == 0) 
        {
            JOptionPane.showMessageDialog(null, "No device is connected", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        Device device = Device.open(devicesInfo.get(0).getUri());
        UserTracker tracker = UserTracker.create();

        try 
        {
            // Set System L&F
            UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException e)
        {
           // handle exception
        }
        catch (ClassNotFoundException e) 
        {
           // handle exception
        }
        catch (InstantiationException e) 
        {
           // handle exception
        }
        catch (IllegalAccessException e) 
        {
           // handle exception
        }

        final UserViewerApplication app = new UserViewerApplication(tracker);
        app.run();
        
    }
}
