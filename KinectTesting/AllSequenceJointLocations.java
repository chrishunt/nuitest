package KinectTesting;   
import com.primesense.nite.UserData; 
import java.io.*; 
import java.util.concurrent.ConcurrentSkipListMap;   
import KinectTesting.*;

/**
 * Class to represent a series of sequences of skeletal movement
 * @author Christopher Hunt
 */
public class AllSequenceJointLocations implements Serializable {   

	//Map of sequence ids mapping to sequences of skeletal movement
	private ConcurrentSkipListMap<Integer,SequenceJointLocations> sequenceJointLocationsMap = new ConcurrentSkipListMap<Integer,SequenceJointLocations>();  

	/**
	 * Gets highest numbered sequence id in the map
	 * @return  Maximum sequence id
	 */
	public int getMaxID() 
	{ 
		if (sequenceJointLocationsMap.size() > 0) 
			return sequenceJointLocationsMap.lastKey(); 
		else 
			return 0; 
	}   

	/**
	 * Adds new sequence to the map of sequences
	 * @return  Maximum sequence id
	 */
	public int addSequenceJointLocations() 
	{ 
		int id = getMaxID() + 1; 
		sequenceJointLocationsMap.put(id, new SequenceJointLocations()); 
		return id; 
	}   

	/**
	 * Gets joint locations of the specified sequence
	 * @param   id  Id of sequence to retrieve joint locations of
	 * @return  Desired sequence of joint locations
	 */
	public SequenceJointLocations getSequenceJointLocations(int id) 
	{ 
		return sequenceJointLocationsMap.get(id); 
	}   

	/**
	 * Updates joint locations of desired sequence
	 * @param   id        Id of sequence to update joint locations of
	 * @param   userData  UserData of the frame containing joint positions
	 */
	public void updateSequenceJointLocations(int id, UserData userData) 
	{
		//Get sequence from map, update it, then place it in the map
		SequenceJointLocations sequenceJointLocations = getSequenceJointLocations(id); 
		sequenceJointLocations.update(userData); 
		sequenceJointLocationsMap.put(id, sequenceJointLocations);
	}   

	/**
	 * Serializes sequences to KinectTesting/jointlocations.kgt
	 * @throws IOException if an error occurs while serializing
	 */
	public void serialize() throws IOException
	{
		FileOutputStream fileOut = new FileOutputStream("KinectTesting/jointlocations.kgt");
     	ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(this);
		out.close();
		fileOut.close();
	}   
}

