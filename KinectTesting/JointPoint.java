package KinectTesting;   
import com.primesense.nite.Point3D; 
import java.io.Serializable;   

/**
 * Class to represent a point in 3D space.
 * Written as NiTE class Point3D does not permit manual setting of point values and is not serializable
 * @author Christopher Hunt
 */
public class JointPoint implements Serializable 
{   
	private float x; 
	private float y; 
	private float z; 

	/**
	 * Default constructor, sets joint point as impossible values
	 */
	public JointPoint() 
	{ 
		x = -999f; 
		y = -999f; 
		z = -999f; 
	}   

	/**
	 * Sets point value
	 * @param  point  Point to set value of
	 */
	public JointPoint(Point3D<Float> point) 
	{ 
		x = point.getX();
		y = point.getY();
		z = point.getZ();
	}   

	/**
	 * Gets X co-ordinate
	 * @return  X co-ordinate of point
	 */
	public float getX() 
	{ 
		return x; 
	}   

	/**
	 * Gets Y co-ordinate
	 * @return  Y co-ordinate of point
	 */
	public float getY() 
	{ 
		return y; 
	}  

	/**
	 * Gets Z co-ordinate
	 * @return  Z co-ordinate of point
	 */
	public float getZ() { 
		return z; 
	}   

	/**
	 * Checks if the current point has the default error values
	 * @return  If the point is default or not
	 */
	public boolean isDefault() 
	{ 
		return (x == 999f && y == 999f && z == 999f); 
	}	
}
