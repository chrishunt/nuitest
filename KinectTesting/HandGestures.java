
import org.openni.*;
import com.primesense.nite.*;
import java.util.List;

public class HandGestures implements HandTracker.NewFrameListener
{

	private boolean mShouldRun = true;
	private HandTracker mTracker;
	private HandTrackerFrameRef mLastFrame;

	public HandGestures(HandTracker tracker)
	{
		mTracker = tracker;
        mTracker.addNewFrameListener(this);
        //mTracker.startGestureDetection(GestureType.WAVE);
        mTracker.startGestureDetection(GestureType.CLICK);
        mTracker.startGestureDetection(GestureType.HAND_RAISE);
		run();
	}

	void run() 
    {
        while (mShouldRun) 
        {
            try 
            {
                Thread.sleep(20000000);
            } 
            catch (Exception e) 
            {
                e.printStackTrace();
            }
        }
    }

	public void onNewFrame(HandTracker tracker) 
	{
        if (mLastFrame != null) 
        {
            mLastFrame.release();
            mLastFrame = null;
        }
        
        mLastFrame = mTracker.readFrame();

		for (GestureData gesture : mLastFrame.getGestures())
        {
			System.out.println("Found a gesture");
			if (gesture.isComplete())
			{
				try
				{
					System.out.println(gesture.getCurrentPosition());
					short code = mTracker.startHandTracking(gesture.getCurrentPosition());
					System.out.println(code);
				}
				catch(RuntimeException e)
				{
					System.out.println("Goofed");
				}
			}
        }

        for (HandData hand : mLastFrame.getHands())
        {
			System.out.println("Found a hand");

			if (!hand.isTracking())
			{
				System.out.println("Lost hand " + hand.getId());

			}
			else
			{
				if (hand.isNew())
				{
					System.out.println("Found new hand " + hand.getId());
				}
			}
        }
	}
	
	static 
    {
        System.load("C:\\Program Files\\OpenNI2\\Redist\\OpenNI2.dll");
        System.load("C:\\Program Files\\PrimeSense\\NiTE2\\Redist\\NiTE2.dll");
    }

	public static void main(String[] args)
	{
		// initialize OpenNI and NiTE
    	OpenNI.initialize();
        NiTE.initialize();
        
        List<DeviceInfo> devicesInfo = OpenNI.enumerateDevices();
        if (devicesInfo.size() == 0) 
        {
            System.out.println("No device is connected");
            return;
        }
        
        Device device = Device.open(devicesInfo.get(0).getUri());

        
        HandTracker tracker = HandTracker.create();

        HandGestures handGestures = new HandGestures(tracker);
	}
}