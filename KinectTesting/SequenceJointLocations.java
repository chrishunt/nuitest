package KinectTesting;   
import com.primesense.nite.*; 
import java.io.Serializable; 
import java.util.concurrent.ConcurrentHashMap; 
import java.util.concurrent.CopyOnWriteArrayList;  
import KinectTesting.*;

/**
 * Class to represent the series of joint locations of a skeleton in a sequence
 * @author Christopher Hunt
 */
public class SequenceJointLocations implements Serializable {   

	//Map of joint types mapping to a list of tuples containing the confidence and value of a joint position
	private ConcurrentHashMap<JointType,CopyOnWriteArrayList<Tuple<Float,JointPoint>>> jointLocations = new ConcurrentHashMap<JointType,CopyOnWriteArrayList<Tuple<Float,JointPoint>>>(); 

	/**
	 * Default constructor, initialses JointType lists for this sequence
	 */
	public SequenceJointLocations() 
	{ 
		//Iterates across JointTye enum, intitialising list of each in the Map
		for (JointType jointType : JointType.values())
		{ 
			jointLocations.put(jointType, new CopyOnWriteArrayList<Tuple<Float,JointPoint>>()); 
		}   
	}   

	/**
	 * Updates sequence joint locations with the positions contatined in this frames UserData
	 * @param userData   UserData of the current frame
	 */
	public void update(UserData userData) 
	{ 
		//Get skeletal data from user data
		Skeleton skeleton = userData.getSkeleton(); 

		//If this skeleton is tracked update joint posistion lists of each joint type
		if (skeleton.getState() == SkeletonState.TRACKED) 
		{
			for (JointType jointType : JointType.values())
			{  
				updateJointList(skeleton, jointType); 
			}   
		} 
	}   

	/**
	 * Updates specified joint location list
	 * @param skeleton   Skeleton containing posistion data
	 * @param jointType  JointType to save position of
	 */
	protected void updateJointList(Skeleton skeleton, JointType jointType) 
	{
		//Get desired joint from skeleton, and if some confidence exists for its position, save this
		//Otherwise, a default value is set
		SkeletonJoint joint = skeleton.getJoint(jointType); 
		float confidence = joint.getPositionConfidence(); 
		JointPoint point; 
		
		if (confidence != 0.0) 
		{ 
			Point3D pos = joint.getPosition(); 
			point = new JointPoint(pos); 
		} 
		else 
		{ 
			point = new JointPoint(); 
		}

		//Add confidence and joint point to accompnying list in Map
		CopyOnWriteArrayList<Tuple<Float,JointPoint>> jointList = jointLocations.get(jointType); 
		Tuple tuple = new Tuple(confidence, point); 
		jointList.add(tuple); 
		jointLocations.put(jointType, jointList); 
	}   
}
