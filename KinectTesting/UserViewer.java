package KinectTesting;

import KinectTesting.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.awt.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

import org.openni.*;
import com.primesense.nite.*;

import com.thoughtworks.xstream.*;

public class UserViewer extends Component 
                        implements UserTracker.NewFrameListener {
    
    float mHistogram[];
    int[] mDepthPixels;
    UserTracker mTracker;
    UserTrackerFrameRef mLastFrame;
    BufferedImage mBufferedImage;
    int[] mColors;
    //Stores currently observed skeleton ids mapping to their accompnying saved id
    HashMap<Short,Integer> currentlyTrackedSkeletons = new HashMap<Short,Integer>();
    ArrayList<Short> frameObservedSkeletons = new ArrayList<Short>();
    AllSequenceJointLocations sequenceJointLocations;
    XStream xstream;


    public UserViewer(UserTracker tracker) {
    	mTracker = tracker;
        mTracker.addNewFrameListener(this);
        mColors = new int[] { 0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0xFFFFFF00, 0xFFFF00FF, 0xFF00FFFF };
        xstream = new XStream();
        xstream.alias("jointpoint", JointPoint.class);
        xstream.alias("jointtype", JointType.class);
        xstream.alias("allskeletonjointlocations", AllSequenceJointLocations.class);
        xstream.alias("skeletonjointlocations", SequenceJointLocations.class);
        deSerializeJointPositions();
    }

    public void deSerializeJointPositions()
    {
        try
        {
            File serializedFile = new File("KinectTesting/jointlocations.kgt");
            String xmlFilepath = "KinectTesting/jointlocations.xml";
            File xmlFile = new File("KinectTesting/jointlocations.xml");
            if (serializedFile.exists())
            {   
                ObjectInputStream is = new ObjectInputStream(new FileInputStream(serializedFile));     
                sequenceJointLocations = (AllSequenceJointLocations) is.readObject();     
                is.close();    
            }
            else if (xmlFile.exists())
            {
                String xml = Utils.readFile(xmlFilepath);
                sequenceJointLocations = (AllSequenceJointLocations)xstream.fromXML(xml);
            }
            else
            {
                sequenceJointLocations = new AllSequenceJointLocations();
            }
        } 
        catch (Exception e)
        {
            e.printStackTrace();
            sequenceJointLocations = new AllSequenceJointLocations();
        }
    }

    public void serialize() throws IOException
    {
        sequenceJointLocations.serialize();
    }

    public void xmlSave()
    {
        try
        {
            //Parse skeletal joint location information to xml
            String xml = xstream.toXML(sequenceJointLocations);
            File file = new File("KinectTesting/jointlocations.xml");
            Utils.saveFile(file,xml);
        }
        catch (Exception e)
        {
            System.out.println("It's dead Jim.");
            e.printStackTrace();
        }
    }
    
    public void paint(Graphics g) {
        if (mLastFrame == null) {
            return;
        }
        
        int framePosX = 0;
        int framePosY = 0;
        
        VideoFrameRef depthFrame = mLastFrame.getDepthFrame();
        if (depthFrame != null) {
	        int width = depthFrame.getWidth();
	        int height = depthFrame.getHeight();
	        
	        // make sure we have enough room
	        if (mBufferedImage == null || mBufferedImage.getWidth() != width || mBufferedImage.getHeight() != height) {
	            mBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	        }
	        
	        mBufferedImage.setRGB(0, 0, width, height, mDepthPixels, 0, width);
	        
	        framePosX = (getWidth() - width) / 2;
	        framePosY = (getHeight() - height) / 2;

	        g.drawImage(mBufferedImage, framePosX, framePosY, null);
        }
        
        if (mLastFrame.getUsers() != null)
        {
            for (UserData user : mLastFrame.getUsers()) 
            {
            	if (user.getSkeleton().getState() == SkeletonState.TRACKED) 
                {
            		drawLimb(g, framePosX, framePosY, user, JointType.HEAD, JointType.NECK);
            		
            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.LEFT_ELBOW);
            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_ELBOW, JointType.LEFT_HAND);

            		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_SHOULDER, JointType.RIGHT_ELBOW);
            		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_ELBOW, JointType.RIGHT_HAND);

            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.RIGHT_SHOULDER);

            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_SHOULDER, JointType.TORSO);
            		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_SHOULDER, JointType.TORSO);

            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.TORSO);
            		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_HIP, JointType.TORSO);
            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.RIGHT_HIP);

            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_HIP, JointType.LEFT_KNEE);
            		drawLimb(g, framePosX, framePosY, user, JointType.LEFT_KNEE, JointType.LEFT_FOOT);

            		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_HIP, JointType.RIGHT_KNEE);
            		drawLimb(g, framePosX, framePosY, user, JointType.RIGHT_KNEE, JointType.RIGHT_FOOT);
            	}
            }
        }
    }

    private void drawLimb(Graphics g, int x, int y, UserData user, JointType from, JointType to) {
    	com.primesense.nite.SkeletonJoint fromJoint = user.getSkeleton().getJoint(from);
    	com.primesense.nite.SkeletonJoint toJoint = user.getSkeleton().getJoint(to);
    	
    	if (fromJoint.getPositionConfidence() == 0.0 || toJoint.getPositionConfidence() == 0.0) {
    		return;
    	}
    	
    	com.primesense.nite.Point2D<Float> fromPos = mTracker.convertJointCoordinatesToDepth(fromJoint.getPosition());
    	com.primesense.nite.Point2D<Float> toPos = mTracker.convertJointCoordinatesToDepth(toJoint.getPosition());

    	// draw it in another color than the use color
    	g.setColor(new Color(mColors[(user.getId() + 1) % mColors.length]));
    	g.drawLine(x + fromPos.getX().intValue(), y + fromPos.getY().intValue(), x + toPos.getX().intValue(), y + toPos.getY().intValue());
    }
    
    public void onNewFrame(UserTracker tracker) {
        if (mLastFrame != null) {
            mLastFrame.release();
            mLastFrame = null;
        }
        
        mLastFrame = mTracker.readFrame();
        
        // check if any new user detected
        for (UserData user : mLastFrame.getUsers()) {
            short userId = user.getId();
            int id;
        	if (user.isNew()) {
                
        		// start skeleton tracking
        		mTracker.startSkeletonTracking(userId);
                
                // Add to stored skeletons
                id = sequenceJointLocations.addSequenceJointLocations();
                currentlyTrackedSkeletons.put(userId,id);
                frameObservedSkeletons.add(userId);
        	}
            else
            {
                frameObservedSkeletons.add(userId);
                id = currentlyTrackedSkeletons.get(userId);
            }
            //Add current frames skeletal data to record
            sequenceJointLocations.updateSequenceJointLocations(id,user);
        }

        //Remove ids from currently tracked skeletons that are no longer present

        HashMap<Short,Integer> oldCurrentlyTrackedSkeletons = (HashMap<Short,Integer>)currentlyTrackedSkeletons.clone();
        currentlyTrackedSkeletons = new HashMap<Short,Integer>();

        for (Map.Entry<Short, Integer> trackedSkel : oldCurrentlyTrackedSkeletons.entrySet()) 
        {
            short key = trackedSkel.getKey();
            int value = trackedSkel.getValue();

            if (frameObservedSkeletons.contains(key))
            {
                //System.out.println("Deleting "+key+ " from list");
                currentlyTrackedSkeletons.put(key,value);
            }
        }     
           
        VideoFrameRef depthFrame = mLastFrame.getDepthFrame();
        
        if (depthFrame != null) {
        	ByteBuffer frameData = depthFrame.getData().order(ByteOrder.LITTLE_ENDIAN);
            ByteBuffer usersFrame = mLastFrame.getUserMap().getPixels().order(ByteOrder.LITTLE_ENDIAN);
        
	        // make sure we have enough room
	        if (mDepthPixels == null || mDepthPixels.length < depthFrame.getWidth() * depthFrame.getHeight()) {
	        	mDepthPixels = new int[depthFrame.getWidth() * depthFrame.getHeight()];
	        }
        
            calcHist(frameData);
            frameData.rewind();

            int pos = 0;
            while(frameData.remaining() > 0) {
                short depth = frameData.getShort();
                short userId = usersFrame.getShort();
                short pixel = (short)mHistogram[depth];
                int color = 0xFFFFFFFF;
                if (userId > 0) {
                	color = mColors[userId % mColors.length];
                }
                
                mDepthPixels[pos] = color & (0xFF000000 | (pixel << 16) | (pixel << 8) | pixel);
                pos++;
            }
            
            depthFrame.release();
            depthFrame = null;
        }

        repaint();
        frameObservedSkeletons.clear();
    }

    private void calcHist(ByteBuffer depthBuffer) {
        // make sure we have enough room
        if (mHistogram == null) {
            mHistogram = new float[10000];
        }
        
        // reset
        for (int i = 0; i < mHistogram.length; ++i)
            mHistogram[i] = 0;

        int points = 0;
        while (depthBuffer.remaining() > 0) {
            int depth = depthBuffer.getShort() & 0xFFFF;
            if (depth != 0) {
                mHistogram[depth]++;
                points++;
            }
        }

        for (int i = 1; i < mHistogram.length; i++) {
            mHistogram[i] += mHistogram[i - 1];
        }

        if (points > 0) {
            for (int i = 1; i < mHistogram.length; i++) {
                mHistogram[i] = (int) (256 * (1.0f - (mHistogram[i] / (float) points)));
            }
        }
    }
}
