//-----------------------------------------------------------------------
// <copyright file="Skeleton2DDataExtract.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
//
//     Java and OpenNI port written by Christopher Hunt
//     Funded through Microsoft SEIF funding
//     
// </copyright>
//-----------------------------------------------------------------------
package DTWGestureRecognition;
import com.primesense.nite.*;
import DTWGestureRecognition.JointPoint;
   
/**
 * Takes Kinect SDK Skeletal Frame coordinates and converts them intoo a format useful to DTW
 */
public class Skeleton2DdataCoordEventArgs
{
    // Positions of the elbows, and the hands (placed from left to right)
    private JointPoint[] _points;

    /*
     * Initializes a new instance of the Skeleton2DdataCoordEventArgs class
     * @param points  The points we need to handle in this class
     */
    public Skeleton2DdataCoordEventArgs(JointPoint[] points)
    {
        _points = (JointPoint[]) points.clone();
    }

    /**
     * Gets the point at a certain index
     * @param index  The index we wish to retrieve
     * @return The point at the sent index
     */
    public JointPoint getPoint(int index)
    {
        return _points[index];
    }

    /**
     * Gets the coordinates of our points
     * @return The coordinates of our points
     */
    protected double[] getCoords()
    {
        double[] tmp = new double[_points.length * 3];
        for (int i = 0; i < _points.length; i++)
        {
            tmp[3 * i] = _points[i].getX();
            tmp[(3 * i) + 1] = _points[i].getY();
            tmp[(3 * i) + 2] = _points[i].getZ();
        }

        return tmp;
    }
}