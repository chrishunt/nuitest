package DTWGestureRecognition;   
import com.primesense.nite.Point3D; 
import java.io.Serializable;   

/**
 * Class to represent a point in 2D space.
 * Written as NiTE class Point2D does not permit manual setting of point values and is not serializable
 * @author Christopher Hunt
 */
public class JointPoint implements Serializable 
{   
	private float x; 
	private float y;
	private float z;

	/**
	 * Default constructor, sets joint point as impossible values
	 */
	public JointPoint() 
	{ 
		x = -9999f; 
		y = -9999f;
		z = -9999f;
	}   

	/**
	 * Sets point value
	 * @param  point  Point to set value of
	 */
	public JointPoint(Point3D<Float> point) 
	{ 
		x = point.getX();
		y = point.getY();
		z = point.getZ();
	}   

	/**
	 * Sets point value
	 * @param  x  X value of point
	 * @param  y  Y value of point
	 */
	public JointPoint(float x, float y, float z) 
	{ 
		this.x = x;
		this.y = y;
		this.z = z;
	} 

	/**
	 * Sets X co-ordinate
	 * @param x  X co-ordinate of point to set
	 */
	public void setX(float x) 
	{ 
		this.x = x; 
	}   

	/**
	 * Sets Y co-ordinate
	 * @param  y  Y co-ordinate of point to set
	 */
	public void setY(float y) 
	{ 
		this.y = y; 
	}

	/**
	 * Sets Z co-ordinate
	 * @param  z  Z co-ordinate of point to set
	 */
	public void setZ(float z) 
	{ 
		this.z = z; 
	}  

	/**
	 * Gets X co-ordinate
	 * @return  X co-ordinate of point
	 */
	public float getX() 
	{ 
		return x; 
	}   

	/**
	 * Gets Y co-ordinate
	 * @return  Y co-ordinate of point
	 */
	public float getY() 
	{ 
		return y; 
	}  

	/**
	 * Gets Z co-ordinate
	 * @return  Z co-ordinate of point
	 */
	public float getZ() 
	{ 
		return z; 
	}  

	/**
	 * Checks if the current point has the default error values
	 * @return  If the point is default or not
	 */
	public boolean isDefault() 
	{ 
		return (x == 9999f && y == 9999f && z == 9999f); 
	}	
}
