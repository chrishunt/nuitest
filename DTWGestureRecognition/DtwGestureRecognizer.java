//-----------------------------------------------------------------------
// <copyright file="DtwGestureRecognizer.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
//
//     Java and OpenNI port written by Christopher Hunt
//     Funded through Microsoft SEIF funding
//   
// </copyright>
//-----------------------------------------------------------------------

package DTWGestureRecognition;

import java.util.*;

/**
 * Dynamic Time Warping nearest neighbour sequence comparison class.
 * Called 'Gesture Recognizer' but really it can work with any vectors
 */
public class DtwGestureRecognizer
{
    // Size of obeservations vectors.
    private int _dimension;
    // Maximum distance between the last observations of each sequence.
    private double _firstThreshold;
    // Minimum length of a gesture before it can be recognised
    private double _minimumLength;
    // Maximum DTW distance between an example and a sequence being classified.
    private double _globalThreshold;
    // The gesture names. Index matches that of the sequences array in _sequences
    private ArrayList _labels;
    // Maximum vertical or horizontal steps in a row.
    private int _maxSlope;
    // The recorded gesture sequences
    private ArrayList<ArrayList<double[]>> _sequences;

    /**
     * Initializes a new instance of the dtwGestureRecognizer class
     * First DTW constructor
     * @param dim  Vector size
     * @param threshold  Maximum distance between the last observations of each sequence
     * @param firstThreshold  Minimum threshold
     */
    public DtwGestureRecognizer(int dim, double threshold, double firstThreshold, double minLen)
    {
        _dimension = dim;
        _sequences = new ArrayList();
        _labels = new ArrayList();
        _globalThreshold = threshold;
        _firstThreshold = firstThreshold;
        _maxSlope = Integer.MAX_VALUE;
        _minimumLength = minLen;
    }

    /**
     * Initializes a new instance of the DtwGestureRecognizer class
     * Second DTW constructor
     * @param dim  Vector size
     * @param threshold  Maximum distance between the last observations of each sequence
     * @param firstThreshold  Minimum threshold
     * @param ms  Maximum vertical or horizontal steps in a row
     */
    public DtwGestureRecognizer(int dim, double threshold, double firstThreshold, int ms, double minLen)
    {
        _dimension = dim;
        _sequences = new ArrayList();
        _labels = new ArrayList();
        _globalThreshold = threshold;
        _firstThreshold = firstThreshold;
        _maxSlope = ms;
        _minimumLength = minLen;
    }

    /**
     * Add a seqence with a label to the known sequences library.
     * The gesture MUST start on the first observation of the sequence and end on the last one.
     * Sequences may have different lengths.
     * @param seq  The sequence
     * @param lab  Sequence name
     */
    public void addOrUpdate(ArrayList seq, String lab)
    {
        // First we check whether there is already a recording for this label. If so overwrite it, otherwise add a new entry
        int existingIndex = -1;

        for (int i = 0; i < _labels.size(); i++)
        {
            if ((String)_labels.get(i) == lab)
            {
                existingIndex = i;
            }
        }

        // If we have a match then remove the entries at the existing index to avoid duplicates. We will add the new entries later anyway
        if (existingIndex >= 0)
        {
            _sequences.remove(existingIndex);
            _labels.remove(existingIndex);
        }

        // Add the new entries
        _sequences.add(seq);
        _labels.add(lab);
    }

    /**
     * Recognize gesture in the given sequence.
     * It will always assume that the gesture ends on the last observation of that sequence.
     * If the distance between the last observations of each sequence is too great, or if the overall DTW distance between the two sequence is too great, no gesture will be recognized.
     * @param seq  The sequence to recognise
     * @return The recognised gesture name
     */
    public String recognize(ArrayList seq)
    {
        double minDist = Double.POSITIVE_INFINITY;
        String classification = "__UNKNOWN";
        for (int i = 0; i < _sequences.size(); i++)
        {
            ArrayList example = (ArrayList) _sequences.get(i);
            if (dist2((double[]) seq.get(seq.size() - 1), (double[]) example.get(example.size() - 1)) < _firstThreshold)
            {
                double d = dtw(seq, example) / example.size();
                if (d < minDist)
                {
                    minDist = d;
                    classification = (String)_labels.get(i);
                }
            }
        }

        return (minDist < _globalThreshold ? classification : "__UNKNOWN") + " " /*+minDist.ToString()*/;
    }

    /**
     * Retrieves a text represeantation of the label and its associated sequence
     * For use in dispaying debug information and for saving to file
     * @return A String containing all recorded gestures and their names
     */
    public String retrieveText()
    {
        String retStr = "";

        if (_sequences != null)
        {
            // Iterate through each gesture
            for (int gestureNum = 0; gestureNum < _sequences.size(); gestureNum++)
            {
                // Echo the label
                retStr += _labels.get(gestureNum) + "\r\n";

                int frameNum = 0;

                //Iterate through each frame of this gesture
                for (double[] frame : ((ArrayList<double[]>)_sequences.get(gestureNum)))
                {
                    // Extract each double
                    for (double dub : (double[])frame)
                    {
                        retStr += dub + "\r\n";
                    }

                    // Signifies end of this double
                    retStr += "~\r\n";

                    frameNum++;
                }

                // Signifies end of this gesture
                retStr += "----";
                if (gestureNum < _sequences.size() - 1)
                {
                    retStr += "\r\n";
                }
            }
        }

        return retStr;
    }

    /**
     * Compute the min DTW distance between seq2 and all possible endings of seq1.
     * @param seq1  The first array of sequences to compare
     * @param seq2  The second array of sequences to compare
     * @return The best match
     */
    public double dtw(ArrayList seq1, ArrayList seq2)
    {
        // Init
        ArrayList seq1R = new ArrayList(seq1);
        Collections.reverse(seq1R);
        ArrayList seq2R = new ArrayList(seq2);
        Collections.reverse(seq2R);
        double[][] tab = new double[seq1R.size() + 1][seq2R.size() + 1];
        int[][] slopeI = new int[seq1R.size() + 1][seq2R.size() + 1];
        int[][] slopeJ = new int[seq1R.size() + 1][seq2R.size() + 1];

        for (int i = 0; i < seq1R.size() + 1; i++)
        {
            for (int j = 0; j < seq2R.size() + 1; j++)
            {
                tab[i][j] = Double.POSITIVE_INFINITY;
                slopeI[i][j] = 0;
                slopeJ[i][j] = 0;
            }
        }

        tab[0][0] = 0;

        // Dynamic computation of the DTW matrix.
        for (int i = 1; i < seq1R.size() + 1; i++)
        {
            for (int j = 1; j < seq2R.size() + 1; j++)
            {
                if (tab[i][j - 1] < tab[i - 1][j - 1] && tab[i][j - 1] < tab[i - 1][j] && slopeI[i][j - 1] < _maxSlope)
                {
                    tab[i][j] = dist2((double[]) seq1R.get(i - 1), (double[]) seq2R.get(j - 1)) + tab[i][j - 1];
                    slopeI[i][j] = slopeJ[i][j - 1] + 1;
                    slopeJ[i][j] = 0;
                }
                else if (tab[i - 1][ j] < tab[i - 1][j - 1] && tab[i - 1][ j] < tab[i][j - 1] && slopeJ[i - 1][j] < _maxSlope)
                {
                    tab[i][j] = dist2((double[]) seq1R.get(i - 1), (double[]) seq2R.get(j - 1)) + tab[i - 1][j];
                    slopeI[i][j] = 0;
                    slopeJ[i][j] = slopeJ[i - 1][j] + 1;
                }
                else
                {
                    tab[i][j] = dist2((double[]) seq1R.get(i - 1), (double[]) seq2R.get(j - 1)) + tab[i - 1][j - 1];
                    slopeI[i][j] = 0;
                    slopeJ[i][j] = 0;
                }
            }
        }

        // Find best between seq2 and an ending (postfix) of seq1.
        double bestMatch = Double.POSITIVE_INFINITY;
        for (int i = 1; i < (seq1R.size() + 1) - _minimumLength; i++)
        {
            if (tab[i][seq2R.size()] < bestMatch)
            {
                bestMatch = tab[i][ seq2R.size()];
            }
        }

        return bestMatch;
    }

    /**
     * Computes a 1-distance between two observations. (aka Manhattan distance).
     * @param a  Point a (double)
     * @param b  Point b (double)
     * @return Manhattan distance between the two points
     */
    private double dist1(double[] a, double[] b)
    {
        double d = 0;
        for (int i = 0; i < _dimension; i++)
        {
            d += Math.abs(a[i] - b[i]);
        }

        return d;
    }

    /**
     * Computes a 2-distance between two observations. (aka Euclidian distance).
     * @param a  Point a (double)
     * @param b  Point b (double)
     * Euclidian distance between the two points
     */
    private double dist2(double[] a, double[] b)
    {
        double d = 0;
        for (int i=0; i<_dimension; i++)
        {
            d += Math.pow(a[i] - b[i], 2);
        }

        return Math.sqrt(d);
    }
}