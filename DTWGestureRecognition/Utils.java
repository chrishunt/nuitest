package DTWGestureRecognition;

import DTWGestureRecognition.*;
import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.nio.charset.*;

/**
 * General purpose utility class.
 * Contains functions such as filewriting utilities to save time elsewhere
 * @author Christopher Hunt
 */
public class Utils
{
	/**
	 * Saves supplied contents to a file of the given path
	 * @param filepath   filepath to write to
	 * @param content    content to write
	 * @throws IOException if error occurs in writing the file
	 */
	public static void saveFile(String filepath, String content) throws IOException
	{
		File file = new File(filepath);
		saveFile(file,content);
	}

	/**
	 * Saves supplied contents to the given file
	 * @param  file     file to write to
	 * @param  content  content to write
	 * @throws IOException if error occurs in writing the file
	 */
	public static void saveFile(File file, String content) throws IOException
	{
		// if file doesnt exist, then create it
		if (file.exists()) 
		{
			file.delete();
		}
		else
		{
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.close();
		fw.close();
	}

	/**
	 * Reads a file returning the contents as a single string
	 * @param path  filepath of file to read
	 * @return	    conent of the file in string format
	 * @throws IOException if error occurs in reading the file
	 */
	static String readFile(String path) throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return Charset.defaultCharset().decode(ByteBuffer.wrap(encoded)).toString();
	}
}