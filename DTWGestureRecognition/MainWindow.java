//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
//
//     Java and OpenNI port written by Christopher Hunt
//     Funded through Microsoft SEIF funding
//     
// </copyright>
//-----------------------------------------------------------------------

package DTWGestureRecognition;

import DTWGestureRecognition.*;

import java.util.*;
import java.io.*;

import com.sun.java.swing.plaf.motif.MotifLookAndFeel;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.io.IOException;
import javax.swing.*;

import org.openni.Device;
import org.openni.OpenNI;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.openni.*;
import com.primesense.nite.*;

/**
 * GUI window and interaction for DTW Gesture recognition system
 */
public class MainWindow implements ActionListener, SkeletonCoordReadyListener
{    
    // Determines whether program should be running
    private boolean shouldRun = true;
    // Variables for timed tasks
    private Timer timer;
    private TimerTask[] tasks;
    private int curCount;
    // GUI elements
    private Button dtwRead, dtwCapture, dtwStore, dtwLoadFile, dtwSaveToFile, dtwShowGestureText, dtwShowInstructions;
    private JComboBox gestureList;
    // Main GUI frame
    private JFrame mFrame;
    // Viewer for Kinect depth output
    private UserViewer mViewer;
    private JLabel recognisedGesture;
    // Possible gestures to record, can be extended
    private String[] gestureStrings = { 
                                "@Right hand swipe left", "@Right hand swipe right", "@Left hand swipe left",
                                "@Left hand swipe right", "@Two hands zoom in", "@Two hands zoom out",
                                "@Right hand wave", "@Left hand wave", "@Right hand pull down", 
                                "@Left hand pull down", "@Right hand push up", "@Left hand push up",
                                "@Both hands pull down", "@Both hands push up", "@Get on down at the disco!"
                              };
    // How many skeleton frames to ignore (_flipFlop)
    // 1 = capture every frame, 2 = capture every second frame etc.
    private final int IGNORE = 2;
    // How many skeleton frames to store in the _video buffer
    private final int BUFFER_SIZE = 32;
    // The minumum number of frames in the _video buffer before we attempt to start matching gestures
    private final int MINIMUM_FRAMES = 6;
    // The minumum number of frames in the _video buffer before we attempt to start matching gestures
    private final int CAPTURE_COUNTDOWN_SECONDS = 3;
    // Where we will save our gestures to. The app will append a data/time and .txt to this string
    private final String GESTURE_SAVE_FILE_LOCATION = "C:\\gitprojects\\DTWGestureRecognition\\";
    // Where we will save our gestures to. The app will append a data/time and .txt to this string
    private final String GESTURE_SAVE_FILE_NAME_PREFIX = "RecordedGestures";
    // Flag to show whether or not the gesture recogniser is capturing a new pose
    private boolean _capturing;
    // Dynamic Time Warping object
    private DtwGestureRecognizer _dtw;
    // Switch used to ignore certain skeleton frames
    private int _flipFlop;
    // ArrayList of coordinates which are recorded in sequence to define one gesture
    private ArrayList _video;
    // ArrayList of coordinates which are recorded in sequence to define one gesture
    private Date _captureCountdown = new Date();

    /**
     * Initializes a new instance of the MainWindow class
     */ 
    public MainWindow(UserTracker tracker)
    {
        _dtw = new DtwGestureRecognizer(12, 0.6, 2, 2, 10);
        _video = new ArrayList();

        // Update the debug window with Sequences information
        System.out.println(_dtw.retrieveText());

        //Initialise GUI
        mFrame = new JFrame("DTW Gesture Recognition");
        mViewer = new UserViewer(tracker);
        
        // register to key events
        mFrame.addKeyListener(new KeyListener() 
        {
            @Override
            public void keyTyped(KeyEvent arg0) {}
            
            @Override
            public void keyReleased(KeyEvent arg0) {}
            
            @Override
            public void keyPressed(KeyEvent arg0) 
            {
                if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) 
                {
                    shouldRun = false;
                }
            }
        });
        
        // register to closing event
        mFrame.addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent e) 
            {
                shouldRun = false;
            }
        });

        //Define GUI elements
        Panel p1 = new Panel(new GridLayout(7,1));
        dtwRead = new Button("Read");
        dtwRead.setActionCommand("read");
        dtwRead.addActionListener(this);
        p1.add(dtwRead);
        dtwCapture = new Button("Capture");
        dtwCapture.setActionCommand("capture");
        dtwCapture.addActionListener(this);
        p1.add(dtwCapture);
        dtwStore = new Button("Store");
        dtwStore.setActionCommand("store");
        dtwStore.addActionListener(this);
        p1.add(dtwStore);
        dtwLoadFile = new Button("Load gesture file");
        dtwLoadFile.setActionCommand("load");
        dtwLoadFile.addActionListener(this);
        p1.add(dtwLoadFile);
        dtwSaveToFile = new Button("Save to file");
        dtwSaveToFile.setActionCommand("save");
        dtwSaveToFile.addActionListener(this);
        p1.add(dtwSaveToFile);
        dtwShowGestureText= new Button("Show gesture text");
        dtwShowGestureText.setActionCommand("show");
        dtwShowGestureText.addActionListener(this);
        p1.add(dtwShowGestureText);
        dtwShowInstructions = new Button("Instructions");
        dtwShowInstructions.setActionCommand("instructions");
        dtwShowInstructions.addActionListener(this);
        p1.add(dtwShowInstructions);
        mFrame.add("West",p1);

        Panel p2 = new Panel(new GridLayout(1,2));
        JLabel gestureLabel = new JLabel("Gesture: ",JLabel.RIGHT);
        p2.add(gestureLabel);
        gestureList = new JComboBox(gestureStrings);
        gestureList.setSelectedIndex(0);
        gestureList.addActionListener(this);
        p2.add(gestureList);
        mFrame.add("North",p2);

        Panel p3 = new Panel(new GridLayout(1,2));
        JLabel recognisedGestureLabel = new JLabel("Recognised gesture: ",JLabel.RIGHT);
        recognisedGestureLabel.setFont(new Font("Sans", Font.PLAIN, 18));
        p3.add(recognisedGestureLabel);
        recognisedGesture = new JLabel("Unknown",JLabel.LEFT);
        recognisedGesture.setFont(new Font("Sans", Font.PLAIN, 18));
        p3.add(recognisedGesture);
        mFrame.add("South",p3);

        mViewer.setSize(800, 600);
        mFrame.add("Center", mViewer);
        mFrame.setSize(mViewer.getWidth()+200, mViewer.getHeight());
        mFrame.setVisible(true);

        //Defaults into read mode
        dtwReadClick();
    }

    /**
     * Loads required dlls for OpenNI and NiTE
     */ 
    static 
    {
        System.load("C:\\Program Files\\OpenNI2\\Redist\\OpenNI2.dll");
        System.load("C:\\Program Files\\PrimeSense\\NiTE2\\Redist\\NiTE2.dll");
    }


    /**
     * Keeps program running while shouldRun is true
     */ 
    void run() 
    {
        while (shouldRun) 
        {
            try 
            {
                Thread.sleep(200);
            } 
            catch (InterruptedException e) 
            {
                e.printStackTrace();
            }
        }
        mFrame.dispose();
    }

    /**
     * Opens the sent text file and creates a _dtw recorded gesture sequence
     * @param fileLocation Full path to the gesture file
     */
     
    private void loadGesturesFromFile(String fileLocation)
    {
        int itemCount = 0;
        String line = "";
        String gestureName = "";

        // TODO I'm defaulting this to 8 here for now as it meets my current need but I need to cater for variable lengths in the future
        ArrayList frames = new ArrayList();
        double[] items = new double[12];

        BufferedReader br = null;
        // Read the file and display it line by line.
        try
        {
            br = new BufferedReader(new FileReader(fileLocation));

            while ((line = br.readLine()) != null)
            {
                if (line.startsWith("@"))
                {
                    gestureName = line;
                    continue;
                }

                if (line.startsWith("~"))
                {
                    frames.add(items);
                    itemCount = 0;
                    items = new double[12];
                    continue;
                }

                if (!line.startsWith("----"))
                {
                    items[itemCount] = Double.parseDouble(line);
                }

                itemCount++;

                if (line.startsWith("----"))
                {
                    _dtw.addOrUpdate(frames, gestureName);
                    frames = new ArrayList();
                    gestureName = "";
                    itemCount = 0;
                }
            }
        }
        catch (IOException ie)
        {
            ie.printStackTrace();
        }
        finally 
        {
            //Close file
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Performs different operations dependent on button click
     * @param  e  Action event triggered
     */
    public void actionPerformed(ActionEvent e) 
    {
        if (e.getActionCommand().equalsIgnoreCase("read")) 
        {
            dtwReadClick();
        }
        else if (e.getActionCommand().equalsIgnoreCase("capture")) 
        {
            dtwCaptureClick();
        }
        else if (e.getActionCommand().equalsIgnoreCase("store")) 
        {
            dtwStoreClick();
        }
        else if (e.getActionCommand().equalsIgnoreCase("load")) 
        {
            dtwLoadFile();
        }
        else if (e.getActionCommand().equalsIgnoreCase("save")) 
        {
            try 
            {
                dtwSaveToFile();
            }
            catch (IOException ie)
            {
                JOptionPane.showMessageDialog(null, "Error while saving gesture data.", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
        else if (e.getActionCommand().equalsIgnoreCase("show")) 
        {
            dtwShowGestureText();
        }
        else if (e.getActionCommand().equalsIgnoreCase("instructions")) 
        {
            dtwShowInstructionsText();
        }
    }

    /**
     * Harness for program to run from
     * @param args  Program arguments
     */
    public static void main(String[] args)
    {
        // initialize OpenNI and NiTE
        OpenNI.initialize();
        NiTE.initialize();
        
        List<DeviceInfo> devicesInfo = OpenNI.enumerateDevices();
        if (devicesInfo.size() == 0) 
        {
            JOptionPane.showMessageDialog(null, "No device is connected", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        Device device = Device.open(devicesInfo.get(0).getUri());
        UserTracker tracker = UserTracker.create();

        try 
        {
            // Set System L&F
            UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }

        final MainWindow app = new MainWindow(tracker);
        //Listen for incoming skeletal points
        Skeleton2DDataExtract.addListener(app);
        app.run();
    }

    /**
     * Runs every time our 2D coordinates are ready.
     * @param points  Converted points from skeletal frame
     */
    @Override
    public void skeletonCoordReady(Skeleton2DdataCoordEventArgs points)
    {
        // We need a sensible number of frames before we start attempting to match gestures against remembered sequences
        if (_video.size() > MINIMUM_FRAMES && _capturing == false)
        {
            String s = _dtw.recognize(_video);
            if (!s.contains("__UNKNOWN"))
            {
                System.out.println("Recognised as: " + s);
                recognisedGesture.setText(s);
                // There was no match so reset the buffer
                _video = new ArrayList();
            }
        }
        
        // Ensures that we remember only the last x frames
        if (_video.size() > BUFFER_SIZE)
        {
            // If we are currently capturing and we reach the maximum buffer size then automatically store
            if (_capturing)
            {
                dtwStoreClick();
            }
            else
            {
                // Remove the first frame in the buffer
                _video.remove(0);
            }
        }

        // Decide which skeleton frames to capture. Only do so if the frames actually returned a number.
        if (!Double.isNaN(points.getPoint(0).getX()))
        {
            // Optionally register only 1 frame out of every n
            _flipFlop = (_flipFlop + 1) % IGNORE;
            if (_flipFlop == 0)
            {
                _video.add(points.getCoords());
            }
        }
    }

    /**
     * Read mode. Sets our control variables and button enabled states
     */
    private void dtwReadClick()
    {
        // Set the buttons enabled state
        dtwRead.setEnabled(false);
        dtwCapture.setEnabled(true);
        dtwStore.setEnabled(false);

        // Set the capturing? flag
        _capturing = false;

        // Update the status display
        System.out.println("Reading");
    }

    /**
     * Starts a countdown timer to enable the player to get in position to record gestures
     */
    private void dtwCaptureClick()
    {
        try
        {
            System.out.println("Get into position");

            //Really grim code to do a 3 second countdown without freezing the GUI
            //And when I say grim, DAMN do I mean grim
            //Like, it seems to report a random stream of numbers when counting down, but somehow still takes 3 seconds
            //WIZARDS.
            timer = new Timer();
            tasks = new TimerTask[4];
            curCount = 4;

            for (int i = 0; i < 4; i++) {
                final int count = i;
                tasks[i] = new TimerTask() {
                    public void run() {
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                if (count < curCount)
                                {
                                    JOptionPane.getRootFrame().dispose();  
                                    curCount = count;
                                    if (count != 0)
                                        JOptionPane.showMessageDialog(null, "Capturing gesture in "+count, "Countdown", JOptionPane.INFORMATION_MESSAGE);
                                }
                            }
                        });
                    }
                };
            }

            for (int i = 0; i < 4; i++) {
                timer.schedule(tasks[4 - i - 1], (1000 * i), (1000 * (i + 1)));
            }

            // After timer finished start capturing gesture
            TimerTask taskRecordGesture = new TimerTask() {
                public void run() {
                    timer.cancel();
                    System.out.println("Recording gesture");
                    startCapture();
                }
            };
            // and schedule it to happen after ROUGHLY 3 seconds
            timer.schedule(taskRecordGesture, 3000);

            
        }
        catch(Exception e)
        {
            System.out.println("Countdown interrupted");
        }
    }

    /**
     * Capture mode. Sets our control variables and button enabled states
     */
    private void startCapture()
    {
        //Can't capture if no skeleton in frame
        if (!mViewer.isTrackedSkeleton())
        {
            //In seperate thread to avoid freezing of GUI while error message is displaying
            Thread t = new Thread(new Runnable(){
                public void run(){
                    JOptionPane.showMessageDialog(null, "No skeleton in view, cannot record gesture.", "Cannot record gesture", JOptionPane.ERROR_MESSAGE);
                }
            });
            t.start();
            return;
        }

        // Set the buttons enabled state
        dtwRead.setEnabled(false);
        dtwCapture.setEnabled(false);
        dtwStore.setEnabled(true);

        // Set the capturing? flag
        _capturing = true;

        //Get selected gesture to save
        String selected = String.valueOf(gestureList.getSelectedItem());

        System.out.println("Recording gesture" + selected);

        // Clear the _video buffer and start from the beginning
        _video = new ArrayList();
    }

    /**
     * Stores our gesture to the DTW sequences list
     */
    private void dtwStoreClick()
    {
        // Set the buttons enabled state
        dtwRead.setEnabled(false);
        dtwCapture.setEnabled(true);
        dtwStore.setEnabled(false);

        // Set the capturing? flag
        _capturing = false;

        String selected = String.valueOf(gestureList.getSelectedItem());

        System.out.println("Remembering " + selected);

        // Add the current video buffer to the dtw sequences list
        _dtw.addOrUpdate(_video, selected);
        //In seperate thread to avoid freezing of GUI while error message is displaying
        Thread t = new Thread(new Runnable(){
            public void run(){
                JOptionPane.showMessageDialog(null, "Gesture successfully recorded.", "Gesture Recorded", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        t.start();
        // Scratch the _video buffer
        _video = new ArrayList();

        // Switch back to Read mode
        dtwReadClick();
    }

    /**
     * Stores our gesture to the DTW sequences list
     */
    private void dtwSaveToFile() throws IOException
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
        //get current date time with Date()
        Date date = new Date();
        File file = new File(GESTURE_SAVE_FILE_LOCATION + GESTURE_SAVE_FILE_NAME_PREFIX + dateFormat.format(date) + ".txt");
        Utils.saveFile(file, _dtw.retrieveText());
        System.out.println("Saved to " + file.getName());
    }

    /**
     * Loads the user's selected gesture file
     */
    private void dtwLoadFile()
    {
        FileDialog fd = new FileDialog(mFrame, "Choose a file", FileDialog.LOAD);
        fd.setDirectory(GESTURE_SAVE_FILE_LOCATION);
        fd.setFile("*.txt");
        fd.setVisible(true);
        String filename = fd.getDirectory() + fd.getFile();
        if (fd.getFile() == null)
        {
            return;
        }
        else
        {
            // Open document
            loadGesturesFromFile(filename);
            System.out.println(_dtw.retrieveText());
            System.out.println("Gestures loaded!");
        }
    }

    /**
     * Stores our gesture to the DTW sequences list
     */
    private void dtwShowGestureText()
    {
        System.out.println(_dtw.retrieveText());
    }

    /**
     * Displays general program operating instructions
     */
    private void dtwShowInstructionsText()
    {
        String instructions = "1. To start, step into frame and allow the Kinect to track your skeleton\n" +
                              "2. Select a gesture you wish to record from the drop down menu\n" +
                              "3. Perform a gesture over 32 frames (a message will pop up showing when complete)\n" +
                              "4. Your gesture will be stored automatically and the system will return to read mode\n" +
                              "5. Test your gesture. If unhappy, you may re-record it to overwrite the previous version\n" +
                              "6. Record some other gestures and test them\n" +
                              "7. Optionally save your gestures for future use\n" +
                              "8. Improve this script, add your own gestures and enjoy the Kinect!\n";

        JOptionPane.showMessageDialog(null, instructions, "Instructions", JOptionPane.INFORMATION_MESSAGE);
    }
}