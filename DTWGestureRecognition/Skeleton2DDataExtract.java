//-----------------------------------------------------------------------
// <copyright file="Skeleton2DDataExtract.cs" company="Rhemyst and Rymix">
//     Open Source. Do with this as you will. Include this statement or 
//     don't - whatever you like.
//
//     No warranty or support given. No guarantees this will work or meet
//     your needs. Some elements of this project have been tailored to
//     the authors' needs and therefore don't necessarily follow best
//     practice. Subsequent releases of this project will (probably) not
//     be compatible with different versions, so whatever you do, don't
//     overwrite your implementation with any new releases of this
//     project!
//
//     Enjoy working with Kinect!
//
//     Java and OpenNI port written by Christopher Hunt
//     Funded through Microsoft SEIF funding
//     
// </copyright>
//-----------------------------------------------------------------------

package DTWGestureRecognition;

import com.primesense.nite.*;
import DTWGestureRecognition.JointPoint;
import java.util.List;
import java.util.ArrayList;

interface SkeletonCoordReadyListener
{
    public void skeletonCoordReady(Skeleton2DdataCoordEventArgs points);
}

/**
 * This class is used to transform the data of the skeleton
 */
public class Skeleton2DDataExtract
{
    //List of listeners to this class
    private static List<SkeletonCoordReadyListener> listeners = new ArrayList<SkeletonCoordReadyListener>();

    /**
     * Adds class to list of listeners to be notified
     * @param toAdd  listener to add
     */
    public static void addListener(SkeletonCoordReadyListener toAdd) {
        listeners.add(toAdd);
    }

    /**
     * Crunches Kinect SDK's Skeleton Data and spits out a format more useful for DTW
     * @param skeleton  Kinect SDK's Skeleton Data
     */
    public static void processData(Skeleton skeleton)
    {
        // Extract the coordinates of the points.
        JointPoint[] p = new JointPoint[4];
        JointPoint shoulderRight = new JointPoint(), shoulderLeft = new JointPoint();
        for (JointType jointType : JointType.values())
        {
            SkeletonJoint joint = skeleton.getJoint(jointType); 
            Point3D<Float> pos = joint.getPosition();
            switch (jointType)
            {
                case LEFT_HAND:
                    p[0] = new JointPoint(pos.getX(), pos.getY(), pos.getZ());
                    break;
                case LEFT_ELBOW:
                    p[1] = new JointPoint(pos.getX(), pos.getY(), pos.getZ());
                    break;
                case RIGHT_ELBOW:
                    p[2] = new JointPoint(pos.getX(), pos.getY(), pos.getZ());
                    break;
                case RIGHT_HAND:
                    p[3] = new JointPoint(pos.getX(), pos.getY(), pos.getZ());
                    break;
                case LEFT_SHOULDER:
                    shoulderLeft = new JointPoint(pos.getX(), pos.getY(), pos.getZ());
                    break;
                case RIGHT_SHOULDER:
                    shoulderRight = new JointPoint(pos.getX(), pos.getY(), pos.getZ());
                    break;
            }
        }
        
        // Centre the data
        JointPoint center = new JointPoint((shoulderLeft.getX() + shoulderRight.getX()) / 2, (shoulderLeft.getY() + shoulderRight.getY()) / 2, (shoulderLeft.getZ() + shoulderRight.getZ()) / 2);
        for (int i = 0; i < 4; i++)
        {
            float x = p[i].getX() - center.getX();
            p[i].setX(x);
            float y = p[i].getY() - center.getY();
            p[i].setY(y);
            float z = p[i].getZ() - center.getZ();
            p[i].setZ(z);
        }

        // Normalization of the coordinates
        double shoulderDist = Math.sqrt(Math.pow((shoulderLeft.getX() - shoulderRight.getX()), 2) +
                                        Math.pow((shoulderLeft.getY() - shoulderRight.getY()), 2) +
                                        Math.pow((shoulderLeft.getZ() - shoulderRight.getZ()), 2));

        for (int i = 0; i < 4; i++)
        {
            float x = p[i].getX() / (float)shoulderDist;
            p[i].setX(x);
            float y = p[i].getY() / (float)shoulderDist;
            p[i].setY(y);
            float z = p[i].getZ() / (float)shoulderDist;
            p[i].setZ(z);
        }
        
        //Notify listeners with point
        for (SkeletonCoordReadyListener l : listeners)
            l.skeletonCoordReady(new Skeleton2DdataCoordEventArgs(p));

    }
}
